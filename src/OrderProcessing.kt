class OrderProcessing {
    /*** Basisstruktur für verkettete Liste ***/
    // Erstes Element der verketten Liste
    var first: OrderNode? = null

    // Ein Knoten der verketteten Liste
    data class OrderNode(val order: Order, var next: OrderNode?)

    /*** Eigenschaften ***/
    // ist die Liste leer?
    val isEmpty: Boolean
    get() {
        return first == null
    }

    // Sind die Items absteigend sortiert?
    fun isSorted(): Boolean
    {
        var node = first
        while (node?.next != null) {
            if(node.order.shoppingCart.totalPrice < node.next?.order?.shoppingCart?.totalPrice!!) {
                return false
            }
            node = node.next
        }
        return true
    }

    // Berechnet den Gesamtwert aller Bestellungen
    val totalVolume: Double
    get() {
        var node = first
        var total = 0.0
        while (node != null) {
            total += node.order.shoppingCart.totalPrice
            node = node.next
        }
        return total
    }

    // Anzahl der Bestellungen
    val size: Int
    get() {
        var order = first
        var count = 0
        while (order != null) {
            count++
            order = order.next
        }
        return count
    }

    // ** Funktionen zum Einfügen **

    // Bestellung hinten anhängen
    fun append(order: Order?) {
        if(order == null) return
        if(first == null) {
            first = OrderNode(order, null)
            return
        }

        var node = first
        loop@ while (true) {
            if(node?.next == null) break@loop
            node = node.next
        }
        node?.next = OrderNode(order, null)
    }

    // Sortiert die Bestellung ein. Details siehe Aufgabentext
    fun insertBeforeSmallerVolumes(order: Order) {
        if(first == null){
            first = OrderNode(order, null)
            return
        }
        if(first?.order?.shoppingCart?.totalPrice!! < order.shoppingCart.totalPrice) {
            first =  OrderNode(order, first)
            return
        }
        var node = first
        while (node?.next != null) {
            if(node.next?.order?.shoppingCart?.totalPrice!! < order.shoppingCart.totalPrice) {
                node.next = OrderNode(order, node.next)
                return
            }
            node = node.next
        }
        append(order)
    }

    // Sortiert nach Auftragsvolumen
    fun sortyByVolume() {
        val new = OrderProcessing()
        repeat(size + 1) {
            var highest = getHighesByBolume()
            print(highest.toString())
            new.append(highest?.order)
        }
        first = new.first
    }

    private fun getHighesByBolume(): OrderNode?
    {
        val highest = findHighestByVolume()
        //print(highest)
        var node = first
        if(first == highest) {
            first = highest?.next
            return highest
        }
        while (node != null) {
            if(node.next == highest) {
                node.next = highest?.next
                highest?.next = null
                return highest
            }
            node = node.next
        }
        return null
    }

    private fun findHighestByVolume(): OrderNode?
    {
        var node = first
        var highest = first
        while (node != null) {
            if(node.order.shoppingCart.totalPrice > highest?.order?.shoppingCart?.totalPrice!!) {
                highest = node
            }
            node = node.next
        }
        return highest
    }

    // Funktionen zum Verarbeiten der Liste

    // Verarbeitet die erste Bestellung und entfernt diese aus der Liste
    fun processFirst() {
        first = first?.next
    }

    // Vearbeitet die Bestellung mit dem höchsten Auftragsvolumen
    // und entfernt diese aus der Liste
    fun processHighest() {
        var highest = first
        var bevorHighest: OrderNode? = null
        var node = first
        while (node?.next != null) {
            if(node.next?.order?.shoppingCart?.totalPrice!! > highest?.order?.shoppingCart?.totalPrice!!) {
                bevorHighest = node
                highest = node.next
            }
            node = node.next
        }
        if (bevorHighest == null) {
            first = first?.next
            return
        }
        bevorHighest.next = highest?.next
    }

    // Verarbeitet alle Aufträge für die Stadt in einem Rutsch
    // und entfernt diese aus der Lite
    fun processAllFor(city: String) {
        var node = first
        if(first?.order?.address?.city == city) {
            first = first?.next
            return processAllFor(city)
        }
        repeat(size) {
            if (node?.next?.order?.address?.city == city) {
                node?.next = node?.next?.next
            } else {
                node = node?.next
            }

        }
    }

    // Verarbeite alle Bestellungen. Die Liste ist danach leer.
    fun processAll() {
        var node = first
        repeat(size) {
            node?.order?.shoppingCart?.buyEverything()
            node = node?.next
        }
        first = null
    }

    // ** Funktionen zum Analysieren**

    // Analysiert alle order mit der analyzer Funktion
    fun analyzeAll(analyzer: (Order) -> String): String {
        var node = first
        var str = ""
        repeat(size) {
            str += analyzer(node?.order!!)
            node = node?.next
        }
        return str
    }

    // Prüft, ob für ein Produkt einer der Bestellungen
    // die predicate Funktion erfüllt wird
    fun anyProduct(predicate: (Product) -> Boolean): Boolean
    {
        var node = first
        while(node != null) {
            val itemList = node.order.shoppingCart.getAllProducts()
            itemList.forEach{
                if(predicate(it.product)) {
                    return true
                }
            }
            node = node.next
        }
        return false
    }

    // Erzeugt ein neues OrderProcessing Objekt, in dem nur noch
    // Order enthalten, für die die predicate Funktion true liefert
    fun filter(predicate: (Order) -> Boolean): OrderProcessing {
        var node = first
        val newList = OrderProcessing()
        while (node != null) {
            if(predicate(node.order)) {
                newList.append(node.order)
            }
            node = node.next
        }
        return newList
    }
}
