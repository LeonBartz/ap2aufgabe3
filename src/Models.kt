// TODO Ersetzen Sie alle Klassen mit Ihrer Implementierung aus Aufgabe 2!

import kotlin.math.round
import kotlin.collections.mutableListOf as mutableListOf
open class Product constructor (var name: String, val buyPrice : Double, val salesPrice : Double, val description : String)
{

    open var basePrice : Double = 0.0

    var stockUnits = mutableListOf<StockUnit>()

    fun profitPerItem() : Double
    {
        return salesPrice - basePrice
    }

    fun valueOfAllItems() : Double
    {
        return availableItems() * basePrice
    }

    fun salesValueOfAllItems() : Double
    {
        return availableItems() * salesPrice
    }

    fun availableItems() : Int
    {
        var count = 0
        for (stockUnit in stockUnits)
        {
            count += stockUnit.quantity
        }
        return count
    }

    override fun toString() : String
    {
        return "${availableItems()} x ${name} Preis: ${salesPrice.round(2)}($basePrice) Euro.\n ${description}\n"
    }

    fun isPreferredQuantityAvailable( preferredQuantity : Int) : Boolean
    {
        return availableItems() >= preferredQuantity
    }

    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        repeat(decimals) { multiplier *= 10 }
        return round(this * multiplier) / multiplier
    }
}


class StockUnit(quantity : Int) {
    var quantity : Int = quantity
        get() = field
        set(value) {field = value}

    var daysBeforExpiration: Int = 0
        get() = field
        set(value) {field = value}

    val isExpired : Boolean
        get() = daysBeforExpiration < 1

    val isExpiringSoon: Boolean
        get() = daysBeforExpiration < 7
}


class ShoppingCart {

    private val productAndQuantityList = mutableListOf<ShoppingCartEntry>()

    val amount: Int
    get() {
        var total = 0
        for (product in productAndQuantityList) {
            total += product.amount
        }
        return total
    }

    fun getAllProducts(): MutableList<ShoppingCartEntry> {
        return productAndQuantityList
    }

    fun addProduct(product: Product, quantity: Int): Boolean
    {
        val bool = productAndQuantityList.add(ShoppingCartEntry(product, quantity))
        return bool
    }

    val allProductsAvailable : Boolean
        get() {
            for (entry in productAndQuantityList)
            {
                if(!entry.productAvailable()) return false
            }
            return true
        }

    val totalPrice : Double
        get() {
            var total = 0.0
            for(entry in productAndQuantityList)
            {
                total += entry.price()
            }
            return total
        }


    fun buyEverything()
    {
        return
    }

    fun clear() {
        productAndQuantityList.clear()
    }


    fun Double.round(decimals: Int): Double {
        var multiplier = 1.0
        repeat(decimals) { multiplier *= 10 }
        return kotlin.math.round(this * multiplier) / multiplier
    }
}




