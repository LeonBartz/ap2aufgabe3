class ShoppingCartEntry(var product : Product, var amount : Int) {

    fun productAvailable() : Boolean
    {
        return product.availableItems() >= amount
    }

    fun price() : Double
    {
        return product.salesPrice * amount
    }

    fun total() : Double
    {
        return price() * amount
    }
}
